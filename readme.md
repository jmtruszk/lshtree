# lshtree: Ultra-fast phylogenetic reconstruction using locality-sensitive hashing #

This program reconstructs phylogenies from multiple sequence alignments containing up to tens or hundreds of thousands of taxa, using an algorithm described in [1] and [2]. It uses locality-sensitive hashing(LSH)[3] to efficiently find neighbouring sequences without the need to compute the full distance matrix.

## Requirements: ##

- g++ (I used gcc version 5.4.0)

## Usage: ##

To build, just type

`make`

To run, type

`lshtree fasta_file > output_file`

## File formats ##

Only fasta files are supported at present. The resulting tree is printed in Newick format.

## Please cite: ##

[1] Brown D.G., Truszkowski J.  Fast phylogenetic tree reconstruction using locality-sensitive hashing. In International Workshop on Algorithms in Bioinformatics (pp. 14-29). Springer, Berlin, Heidelberg.

## References: ##

[1] Brown D.G., Truszkowski J.  Fast phylogenetic tree reconstruction using locality-sensitive hashing. In International Workshop on Algorithms in Bioinformatics (pp. 14-29). Springer, Berlin, Heidelberg, 2012.

[2] Truszkowski J. Fast Algorithms for Large-Scale Phylogenetic Reconstruction. PhD thesis, University of Waterloo, 2013.

[3] Indyk, P., & Motwani, R.  Approximate nearest neighbors: towards removing the curse of dimensionality. In Proceedings of the thirtieth annual ACM symposium on Theory of computing (pp. 604-613), 1998.


