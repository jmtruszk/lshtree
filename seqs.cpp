#ifndef SEQS_CPP
#define SEQS_CPP



#include"seqs.h"

// DEFINITIONS ... random stuff

double expected=-0.52;

#ifndef _BLOSUM
#define _BLOSUM
static int BLOSUM62[24][24]=
{
	4,-1,-2,-2,0,-1,-1,0,-2,-1,-1,-1,-1,-2,-1,1,0,-3,-2,0,-2,-1,0,-4,
 -1,5,0,-2,-3,1,0,-2,0,-3,-2,2,-1,-3,-2,-1,-1,-3,-2,-3,-1,0,-1,-4,
 -2,0,6,1,-3,0,0,0,1,-3,-3,0,-2,-3,-2,1,0,-4,-2,-3,3,0,-1,-4,
 -2,-2,1,6,-3,0,2,-1,-1,-3,-4,-1,-3,-3,-1,0,-1,-4,-3,-3,4,1,-1,-4,
 0,-3,-3,-3,9,-3,-4,-3,-3,-1,-1,-3,-1,-2,-3,-1,-1,-2,-2,-1,-3,-3,-2,-4,
 -1,1,0,0,-3,5,2,-2,0,-3,-2,1,0,-3,-1,0,-1,-2,-1,-2,0,3,-1,-4,
 -1,0,0,2,-4,2,5,-2,0,-3,-3,1,-2,-3,-1,0,-1,-3,-2,-2,1,4,-1,-4,
 0,-2,0,-1,-3,-2,-2,6,-2,-4,-4,-2,-3,-3,-2,0,-2,-2,-3,-3,-1,-2,-1,-4,
 -2,0,1,-1,-3,0,0,-2,8,-3,-3,-1,-2,-1,-2,-1,-2,-2,2,-3,0,0,-1,-4,
 -1,-3,-3,-3,-1,-3,-3,-4,-3,4,2,-3,1,0,-3,-2,-1,-3,-1,3,-3,-3,-1,-4,
 -1,-2,-3,-4,-1,-2,-3,-4,-3,2,4,-2,2,0,-3,-2,-1,-2,-1,1,-4,-3,-1,-4,
 -1,2,0,-1,-3,1,1,-2,-1,-3,-2,5,-1,-3,-1,0,-1,-3,-2,-2,0,1,-1,-4,
 -1,-1,-2,-3,-1,0,-2,-3,-2,1,2,-1,5,0,-2,-1,-1,-1,-1,1,-3,-1,-1,-4,
 -2,-3,-3,-3,-2,-3,-3,-3,-1,0,0,-3,0,6,-4,-2,-2,1,3,-1,-3,-3,-1,-4,
 -1,-2,-2,-1,-3,-1,-1,-2,-2,-3,-3,-1,-2,-4,7,-1,-1,-4,-3,-2,-2,-1,-2,-4,
 1,-1,1,0,-1,0,0,0,-1,-2,-2,0,-1,-2,-1,4,1,-3,-2,-2,0,0,0,-4,
 0,-1,0,-1,-1,-1,-1,-2,-2,-1,-1,-1,-1,-2,-1,1,5,-2,-2,0,-1,-1,0,-4,
 -3,-3,-4,-4,-2,-2,-3,-2,-2,-3,-2,-3,-1,1,-4,-3,-2,11,2,-3,-4,-3,-2,-4,
 -2,-2,-2,-3,-2,-1,-2,-3,2,-1,-1,-2,-1,3,-3,-2,-2,2,7,-1,-3,-2,-1,-4,
 0,-3,-3,-3,-1,-2,-2,-3,-3,3,1,-2,1,-1,-2,-2,0,-3,-1,4,-3,-2,-1,-4,
 -2,-1,3,4,-3,0,1,-1,0,-3,-4,0,-3,-3,-2,0,-1,-4,-3,-3,4,1,-1,-4,
 -1,0,0,1,-3,3,4,-2,0,-3,-3,1,-1,-3,-1,0,-1,-3,-2,-2,1,4,-1,-4,
 0,-1,-1,-1,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,0,0,-2,-1,-1,-1,-1,-1,-4,
 -4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,1,
};
#endif

bool operator==(seqpair s1,seqpair s2)
{
	return (s1.a==s2.a)&&(s1.b==s2.b);
}

int log2TotalTaxaNo=18; //set that before calling the hash fun


// DEFINITIONS... Sequences


Sequences::Sequences()
{
	for(int i=0;i<256;i++)
		ALPHABET[i]=-1;
	for(int i=0;i<24;i++)
		ALPHABET[(int)REVTAB[i]]=i;
	char gap1='-';
	char gap2='.';
	ALPHABET[(int)gap1]=GAP_VALUE;
	ALPHABET[(int)gap2]=GAP_VALUE;
}
	
int Sequences::read(string file)
{
	sequences.clear();
	names.clear();
	fstream f(file.c_str(),ios::in);
	string name;
	string seq="";
	string line="";
	while(f>>line)
	{
		if(line[0]=='>')
		{
			names.push_back(line.substr(1,line.size()));// this is to please the phylip format
			if(!seq.empty())
					//sequences.push_back(seq);
				appendSequence(seq);
			seq.clear();
		}
		else
		{
			for(int i=0;i<line.size();i++)
				line[i]=toupper(line[i]);
			seq+=line;
		}
				
	}
		//sequences.push_back(seq);
	appendSequence(seq);
	if(sequences.size()!=names.size())
		return -1;
	return 0;
		
}
	
int Sequences::getNumberOfSequences()
{
	return sequences.size();
}
	
bool Sequences::isGap(int seqno,int pos)
{
	return sequences[seqno][pos]==GAP_VALUE;
}
	
void Sequences::appendSequence(string seq) // converts the sequence to ints and appends it at the end of the sequence list
{
	vector<int> newseq(seq.size(),-1);
	for(int i=0;i<seq.size();i++)
		newseq[i]=ALPHABET[(int)seq[i]];
	sequences.push_back(newseq);
}



// definitions BItSequences



BitSequences::BitSequences()
{
	for(int i=0;i<256;i++)
		ALPHABET[i]=-1;
	for(int i=0;i<4;i++)
		ALPHABET[(int)REVTABNUC[i]]=i;
	char gap1='-';
	char gap2='.';
	ALPHABET[(int)gap1]=GAP_VALUE;
	ALPHABET[(int)gap2]=GAP_VALUE;
			
	for(int i=0;i<65536;i++) //initialize the lookup table for distance computations
	{
		int rawcnt=0;
		for(int j=0;j<8;j++)
		{
			if((i&bitmask[j])!=0)
				rawcnt++;
		}
		nDiffs[i]=rawcnt;
	}
}
	



int BitSequences::read(string file)
{
	sequences.clear();
	names.clear();
	fstream f(file.c_str(),ios::in);
	string name;
	string seq="";
	string line="";
	while(f>>line)
	{
		if(line[0]=='>')
		{
			names.push_back(line.substr(1,line.size()));// this is to please the phylip format
			if(!seq.empty())
					//sequences.push_back(seq);
				appendSequence(seq);
			seq.clear();
		}
		else
		{
			for(int i=0;i<line.size();i++)
				line[i]=toupper(line[i]);
			seq+=line;
		}
				
	}
		//sequences.push_back(seq);
	appendSequence(seq);
	if(sequences.size()!=names.size())
		return -1;
	return 0;
		
}
	


int BitSequences::getNumberOfSequences()
{
	return sequences.size();
}
		


int BitSequences::getNumberOfColumns()
{
	return sequences[0].size()*sizeof(short int)*4; //*8bits/2bitsperletter
}
	


bool BitSequences::isGap(int seqno,int pos)
{
	return gaps[seqno][pos>>3]&bitmask[pos&0x00000007]==0;
}
	

void BitSequences::appendSequence(string seq) // converts the sequence to ints and appends it at the end of the sequence list
{
	int shortintsize=seq.size()/8;
	if(seq.size()%8>0)
		shortintsize++;
	vector<unsigned short int> newseq(shortintsize,0);
	vector<unsigned short int> newgap(shortintsize,0xFFFF);
	int bigind,smallind;
	for(int i=0;i<seq.size();i++)
	{
		bigind=i>>3;
		smallind=i%8;
		newseq[bigind]=newseq[bigind]<<2;
				
		if(seq[i]=='-'||seq[i]=='.')
		{
			newgap[bigind]=newgap[bigind]^bitmask[smallind];
			continue; // so that we don't add anything odd to newseq
		}
		newseq[bigind]=newseq[bigind]|ALPHABET[(int)seq[i]];
	}
	for(smallind++;smallind<8;smallind++) // add virtual gaps at the end of the sequence
	{
		newseq[bigind]=newseq[bigind]<<2;
		newgap[bigind]=newgap[bigind]^bitmask[smallind];
	}
	sequences.push_back(newseq);
	gaps.push_back(newgap);
}
		
		
void BitSequences::updateSequence(vector<int> seq,int index) // converts the sequence to ints and appends it at the end of the sequence list
{								// THIS seq is NOT ascii codes! need something different than ALPHABET
	int shortintsize=seq.size()/8;
	if(seq.size()%8>0)
		shortintsize++;
	vector<unsigned short int> newseq(shortintsize,0);
	vector<unsigned short int> newgap(shortintsize,0xFFFF);
	int bigind,smallind;
	for(int i=0;i<seq.size();i++)
	{
		bigind=i>>3;
		smallind=i%8;
		newseq[bigind]=newseq[bigind]<<2;
				
		if(seq[i]==GAP_VALUE)
		{
			newgap[bigind]=newgap[bigind]^bitmask[smallind];
			continue; // so that we don't add anything odd to newseq
		}
		newseq[bigind]=newseq[bigind]|(unsigned short int)seq[i];
	}
	for(smallind++;smallind<8;smallind++) // add virtual gaps at the end of the sequence
	{
		newseq[bigind]=newseq[bigind]<<2;
		newgap[bigind]=newgap[bigind]^bitmask[smallind];
	}
	sequences[index].clear();
	gaps[index].clear();
	sequences[index]=newseq;
	gaps[index]=newgap;
}
		
void BitSequences::addSequence(vector<int> seq)
{
	int index=sequences.size();
	vector<unsigned short int> empty;
	sequences.push_back(empty);
	gaps.push_back(empty);
	updateSequence(seq,index);
}
		
double BitSequences::rawDist(int a,int b)
{
	int rawcnt=0;
	int nongaps=0;
			
	for(int i=0;i<sequences[a].size();i++)
	{
		unsigned short int rawdiff=sequences[a][i]^sequences[b][i]; //we assume that gaps are all labelled as 0
		unsigned short int gappattern=gaps[a][i]&gaps[b][i];
		unsigned short int rawdiffmasked=rawdiff&gappattern;
		rawcnt+=nDiffs[rawdiffmasked];
		nongaps+=nDiffs[gappattern];
	}
	return (double)rawcnt/nongaps;
}
		
		// for backward compatibility -quasi synonym checking
double BitSequences::hammingDist(int a,int b)
{
	int rawcnt=0;
			
	for(int i=0;i<sequences[a].size();i++)
	{
		unsigned short int rawdiff=sequences[a][i]^sequences[b][i]; //we assume that gaps are all labelled as 0
		unsigned short int gappattern=gaps[a][i]&gaps[b][i];
		unsigned short int rawdiffmasked=rawdiff&gappattern;
		rawcnt+=nDiffs[rawdiffmasked];
	}
	return (double)rawcnt;
}
		
		
		// for HT's
		
int BitSequences::getSubseq(int seqno, vector<int> positions)
{
	int sub=0;
	for(int i=0;i<positions.size();i++)
	{
		sub=sub<<2;
		sub=sub|getLetter(seqno,positions[i]);
				
	}
	return sub;
}
		


vector<int> BitSequences::getSequence(int seqno)
{
	vector<int> seq;
	for(int i=0;i<sequences[0].size();i++)
	{
		unsigned short int block=sequences[seqno][i];
		unsigned short int gapblock=gaps[seqno][i];
		for(int j=0;j<8;j++)
		{
			unsigned short int letter=block&bitmask[j];
			unsigned short int isGapp=gapblock&bitmask[j];
					
			int ind=letter>>((7-j)*2);
			int gapind=isGapp>>((7-j)*2);
			if(gapind==0)//gap
				seq.push_back(GAP_VALUE);
			else
				seq.push_back((int)ind);
		}
	}
	return seq;
}






//DEFINITIONS DistanceORacle


DistanceOracle::DistanceOracle(string file,bool corr)
{
	ndistqueries=0;
	whoseturnclear=0;
	correction=corr;
	self_alloc_sequences=true;
	sequences=new BitSequences();
	sequences->read(file);
			
	distCache.resize(sequences->getNumberOfSequences());
			//distCache.init(sequences.getNumberOfSequences());
			
			
			
}

		

DistanceOracle::DistanceOracle(BitSequences* seqs) // just copy for now, will worry about that later...
{
	self_alloc_sequences=false;
	ndistqueries=0;
	whoseturnclear=0;
	sequences=seqs;
	distCache.resize(sequences->getNumberOfSequences());
			
}
		
		

DistanceOracle::~DistanceOracle()
{
	if(self_alloc_sequences)
		delete sequences;
}

int DistanceOracle::incSize()
{
	int curSize=distCache.size();
	distCache.resize(curSize+1);
	return distCache.size();
}

		

bool DistanceOracle::hashCheck(int a,int b)
{
	if(a>b)
	{
		int p=a;
		a=b;
		b=p;
	};
			//if(distCache.check(a,b))
	if(distCache[a].count(b)==1)
		return true;
	return false;
}
		
		
double DistanceOracle::hashGet(int a,int b)
{
	if(a>b)
	{
		int p=a;
		a=b;
		b=p;
	};
			
			//return distCache.getValue(a,b);
	return distCache[a][b];
}
		
		

void DistanceOracle::hashSet(int a,int b,double d)
{
	if(a>b)
	{
		int p=a;
		a=b;
		b=p;
	};
			
			//distCache.storeValue(a,b,d);
	distCache[a][b]=d;
			
}
		
		

void DistanceOracle::hashClear()
{
	for(int i=0;i<distCache.size();i++)
		distCache[i].clear();
}
		
		

void DistanceOracle::hashHalfClear()
{
	for(int i=0;i<distCache.size();i++)
	{
				
		if((i+whoseturnclear)%2==0)
			distCache[i].clear();
	}
	whoseturnclear++;
}
		// no correction...
		// now just the blosum score
		

double DistanceOracle::dist(int a,int b)
{
	nanerror=false;
	//if(hashCheck(a,b))
	//	return hashGet(a,b);
			
	ndistqueries++;
			
	if(ndistqueries>MAX_HT_SIZE)
	{
		hashHalfClear();
		ndistqueries=ndistqueries/2;
	}
			
	double rdn=sequences->rawDist(a,b); // BitSequences does all the hard work
			
	double normed=1.0-1.333333333*rdn;
	double di;
			// exception handling
	if(normed<=0)
		di=5;
	else
		di=-0.75*log(normed);
	if(isnan(di))
	{
		di=5;
		nanerror=true;
	};
	//hashSet(a,b,di); // cache them so that we don't do this again...
	return di;
			
}
		

		

		
		
bool DistanceOracle::quasiSynonymCheck(int a,int b)
{
	int hd=sequences->hammingDist(a,b);
	if(hd<=QUASI_SYNONYM_THRESHOLD)
		return true;
	return false;
}
		
		
vector<vector<double> > DistanceOracle::calcAll()
{
	vector<vector<double> > d(size());
	for(int i=0;i<size();i++)
	{
		d.resize(size());
		d[i][i]=0.0;
	}
				
			
	for(int i=0;i<size();i++)
		for(int j=0;j<i;j++)
	{
		d[i][j]=dist(i,j);
		d[j][i]=d[i][j];
	}
	return d;
					
}
		
		
void DistanceOracle::printToPhylip(string file)
{
	fstream f(file.c_str(),ios::out);
	f<<size();
	f<<"\n";
	vector<vector<double> > d=calcAll();
	for(int i=0;i<size();i++)
	{
		f<<sequences->names[i];
				
		for(int j=0;j<size();j++)
		{
			f<<" ";
			f<<d[i][j];
		}
		f<<"\n";
	}
	f.close();
}
		
// 		double dist(int a,int b)
// 		{
// 			//if(!correction)
// 			//	return rawDist(a,b);
// 			double d=rawDist(a,b);
// 			double adjusted=1.0 - d - (d * d * 0.20) ;
// 			if(adjusted<0)
// 			{
		// 				
// 				cerr<<"ERROR: distance correction error "<<adjusted;
// 				cerr<<"raw distance:"<<d;
// 				cerr<<"sequences:"<<a<<","<<b;
// 				exit(0);
// 			};
// 			double distance = - log( adjusted );
// 			return distance;
// 		}
		
		

int DistanceOracle::size()
{
	return sequences->getNumberOfSequences ();
}


#endif