#include"lhood.h"

#define NNI_INDEP_REPS

using namespace std;




template<class T>
string toString(T t)
{
	stringstream ss;
	ss<<t;
	return ss.str();
}


template<class T,int N>	
void PhyloForest<T,N>::init(int ntaxa,BitSequences* seqs)
{
	badFixCount=0;
	this->ntaxa=ntaxa;
	sequences=seqs;
	componentSizes.resize(ntaxa);
	rootNodes.resize(ntaxa);
	mergeNonRoots=false;
	for(int i=0;i<ntaxa;i++)
	{
		rootNodes[i]=i;
		componentSizes[i]=1; //each leaf in their own component
	}
	nodes.resize(ntaxa);
	initLeaves();
	//initProbs();
	m=sequences->getNumberOfColumns();
	clo=new ClosenessOracle<T,N>();
	clo->init(sequences,this,minMutation,0.6); //off the top of my head...for now
	distOracle=new DistanceOracle(seqs);
	BFSCounter=0;
}
		
		
		
template<class T,int N>			
void PhyloForest<T,N>::initProbs()
{
	for(int i=0;i<ntaxa;i++)
		nodes[i].initProbs(sequences->getSequence(i));
}

template<class T,int N>			
void PhyloForest<T,N>::initLeaves()
{
	
	for(int i=0;i<ntaxa;i++)
	{
		vector<int> seq=sequences->getSequence(i);
		nodes[i].initLeaf(i,seq);
	}
}
		
template<class T,int N>			
void PhyloForest<T,N>::initBFS(int startNode,int searchId) // searchId distinguishes this BFS from all the previous ones
{
	while(!bfsqueue.empty())
		bfsqueue.pop();
	bfsqueue.push(startNode);
	nodes[startNode].curBFS=searchId;
	nodes[startNode].distFromBFSOrigin=0.0;
}
		
		
template<class T,int N>	
int PhyloForest<T,N>::BFSStep(int searchId) //returns the next node encountered 
{
	if(bfsqueue.empty())
		return -1;
	int curNode=bfsqueue.front();
	bfsqueue.pop();
	for(int i=0;i<nodes[curNode].degree;i++)
	{
		int neighbour=nodes[curNode].neighbour[i];
		if(nodes[neighbour].curBFS!=searchId)
		{
			bfsqueue.push(neighbour);
			nodes[neighbour].curBFS=searchId;
		}
	}
	return curNode;
}
		
		
template<class T,int N>			
int PhyloForest<T,N>::sameComponent(int node1,int node2)
{
	return nodes[node1].myTreeId==nodes[node2].myTreeId;
}


// doesn't work for now
// template<class T,int N>			
// int PhyloForest<T,N>::checkSumComponentSizes()
// {
// 	int sum=0;
// 	for(int i=0;i<nodes.size();i++)
// 	{
// 		if(activeComponent[i])
// 		{
// 			sum+=componentSizes[i];
// 		};
// 	}
// 	return sum;
// }
		
		
template<class T,int N>	
int PhyloForest<T,N>::BFSLookUpNext(int searchId)// without fetching from the queue
{
	if(bfsqueue.empty())
		return -1;
	return bfsqueue.front();
}
		

template<class T,int N>	
int PhyloForest<T,N>::BFSBlockExpansion(int node,int searchId)// prevents going further from node
{
	for(int i=0;i<nodes[node].degree;i++)
	{
		nodes[nodes[node].neighbour[i]].curBFS=searchId;
		//nodes[nodes[node].neighbour[i]].distFromBFSOrigin=100000.0; 
		//hope this doesn't mess anything...
	}
}
		
template<class T,int N>	
int PhyloForest<T,N>::findBFSParent(int curNode,int searchId) //works only when curNode has not been expanded yet
{
	for(int i=0;i<nodes[curNode].degree;i++)
	{
		int nbr=nodes[curNode].neighbour[i];
		if(nodes[nbr].curBFS==searchId)
			return nbr;
	}
	return -1;
}
		
		
		
		
template<class T,int N>	
int PhyloForest<T,N>::findPotentialParent(int node) // really, find two neighbours whose reconstructed sequences do not depend on this one, choose the third direction
{
	if(nodes[node].degree<3)
		return -1;
	int nonparents[2];
	int nnon=0;
	for(int i=0;i<nodes[node].degree;i++)
	{
		int othernode=nodes[node].neighbour[i];
		if(nodes[othernode].parent==node)
		{
			nonparents[nnon]=othernode;
			nnon++;
		}
	}
	return nodes[node].findThird(nonparents[0],nonparents[1]);
}
		
		
template<class T,int N>	
int PhyloForest<T,N>::splitEdge(int node1,int node2)// after this, one has to set edge lengths and the ancestral sequence
{
	int ind1=nodes[node1].findPos(node2);
	int ind2=nodes[node2].findPos(node1);
			
	PhyloForestNode<T,N> newnode;
			
	int newnodeind=nodes.size();
			
	nodes[node1].neighbour[ind1]=newnodeind;
	nodes[node2].neighbour[ind2]=newnodeind;
	newnode.addNeighbour(node1);
	newnode.addNeighbour(node2);
	nodes.push_back(newnode);
			//set up parents so that we know how the reconstructions should proceed
	if(nodes[node1].parent==-1)//we're creating a new root now
	{
		nodes[node1].parent=newnodeind;
		nodes[node2].parent=newnodeind;
		nodes[newnodeind].parent=-1;
	}
	else //adding something on the side of the tree
	{
		int older,younger;
		if(nodes[node1].parent==node2)
		{
			older=node2;
			younger=node1;
		}
		else
		{
			older=node1;
			younger=node2;
		}
		nodes[younger].parent=newnodeind;
		nodes[newnodeind].parent=older;
	}
				
	
	// anyhow, we have to set the component number
	nodes[newnodeind].myTreeId=nodes[node1].myTreeId;
			
	return newnodeind;
				
}
		
		
template<class T,int N>	
void PhyloForest<T,N>::addEdge(int node1,int node2)
{
	nodes[node1].addNeighbour(node2);
	nodes[node2].addNeighbour(node1);
	if(nodes[node1].parent!=-1&&nodes[node2].parent==-1)
	{
		nodes[node2].parent=node1;
	}
	if(nodes[node1].parent==-1&&nodes[node2].parent!=-1)
	{
		nodes[node1].parent=node2;
	}
}
		
		
template<class T,int N>	
void PhyloForest<T,N>::setEdgeLength(int node1,int node2,T len)
{
	if(len==minEdgeLen)
	{
		//cerr<<"SUSPICIOUS EDGE:"<<node1<<","<<node2<<"\n";
		Edge se(node1,node2);
		//suspiciousEdges.push_back(se);
	};
	nodes[node1].setEdgeLength(node2,len);
	
	nodes[node2].setEdgeLength(node1,len);
	
			
}
		
		
template<class T,int N>	
int PhyloForest<T,N>::findJuniorNode(Edge e)
{
	if(nodes[e.end1].parent==e.end2)
	{
		return e.end1;
	}
	if(nodes[e.end2].parent==e.end1)
	{
		return e.end2;
	}
	return -1; // both are indep. of each other
			
}
		

template<class T,int N>	
int PhyloForest<T,N>::findSeniorNode(Edge e)
{
	if(nodes[e.end1].parent==e.end2)
	{
		return e.end2;
	}
	if(nodes[e.end2].parent==e.end1)
	{
		return e.end1;
	}
	return -1; // both are indep. of each other		
}
		
		
		
template<class T,int N>			
vector<Edge> PhyloForest<T,N>::findCandidateEdges(int subtreeRoot,int otherHit)
{
	vector<Edge> edges;
	BFSCounter++;
	T maxdist=distOracle->dist(subtreeRoot,otherHit);
	initBFS(otherHit,BFSCounter);
	nodes[otherHit].distFromBFSOrigin=0.0;
	Edge rootEdge(-1,-1);
	int origin=BFSStep(BFSCounter); // get the origin out of the queue
	if(nodes[origin].degree==2)
	{
		rootEdge=getLongEdgeFromRoot(origin);
	};
		
	for(;;)
	{
		int next=BFSLookUpNext(BFSCounter);
		if(next==-1)
			break;
		int prev=findBFSParent(next,BFSCounter);
		Edge e(prev,next);
		if(nodes[next].degree==2)
		{
			rootEdge=getLongEdgeFromRoot(next);
		};
		if(nodes[next].degree!=2&&nodes[prev].degree!=2)
		{
			edges.push_back(e);
		};
		nodes[next].distFromBFSOrigin=nodes[prev].distFromBFSOrigin+nodes[next].bl[nodes[next].findPos(prev)];
		if(nodes[next].distFromBFSOrigin>maxdist)
			BFSBlockExpansion(next,BFSCounter);
		BFSStep(BFSCounter); //finally
	}
	if(rootEdge.end1!=-1)
		edges.push_back(rootEdge);
	if(edges.size()==0)
	{
		Edge e(otherHit,otherHit);
		edges.push_back(e);
	}
	return edges;
}
		

template<class T,int N>	
T PhyloForest<T,N>::tryAttach(Edge e1,Edge e2)
{
	T edgelen= estimateQuartetMidEdge(e1,e2);
	return edgelen; // for now, no checking of any sort
}
		
template<class T,int N>	
Edge PhyloForest<T,N>::getLongEdgeFromRoot(int root)
{
	if(nodes[root].degree==2)
	{
		Edge e(nodes[root].neighbour[0],nodes[root].neighbour[1]);
		return e;
	};
	if(nodes[root].degree==0)
	{
		Edge e(root,root);
		return e;
	};
	Edge e(-1,-1); //error
	return e;
			
}
	
	
template<class T,int N>	
Edge PhyloForest<T,N>::findRootEdgeIfNecessary(Edge newedge)
{
	if(nodes[newedge.end1].parent==-1&&nodes[newedge.end2].parent==-1)// there were no NNI's that would damage the root
		return newedge;
	if(nearRootAffected==-1)//root is somewhere far away and not affected. We don't need to do anything
	{
		Edge e(-1,-1);
		return e;
	};
	int curNode=nearRootAffected;
	while(nodes[curNode].parent!=-1)
	{
		curNode=nodes[curNode].parent;
	}
	if(nodes[curNode].degree==2)//root already done...
	{
		Edge e(curNode,curNode);
		return e;
	};
	Edge e(curNode,-1);
	for(int i=0;i<nodes[curNode].degree;i++)//deg==3
	{
		int nbr=nodes[curNode].neighbour[i];
		if(nodes[nbr].parent==-1)
		{
			e.end2=nbr;
			return e;
		};
	}
	return e;
}


template<class T,int N>	
void PhyloForest<T,N>::tryMerge(int subtreeRoot,int otherHit)
{
	deg2rootexists=true;
	suspiciousEdges.clear();
	vector<Edge> candidates=findCandidateEdges(subtreeRoot,otherHit); // for now, we only go one way
			
	Edge e=getLongEdgeFromRoot(subtreeRoot);
			
	T minlen=1000000.0;
	int min=-1;
	for(int i=0;i<candidates.size();i++)
	{
		T edgelen=tryAttach(e,candidates[i]);
		
		//cerr<<"\ncand. edge:"<<candidates[i].end1<<","<<candidates[i].end2<<"len:"<<edgelen;
	
		if(edgelen<minlen)   // MUST DO SOME OVERLAP CHECKING AT SOME POINT!
		{
			minlen=edgelen;
			min=i;
		};
	}
			
	int juniorNode=findJuniorNode(candidates[min]); // we have to do this before connecting trees or else we'll think it's always root+root
	
	Edge newedge=connectTrees(e,candidates[min]);
			// find which end in candidates[min] depends on the other
	
	
			// now re-estimate branch lengths around
	reEstimateQuartetLengths();
	
	reEstimateSequences();   // HAVE TO UPDATE HASH TABLES WITHIN THIS FUNCTION?????
	
	
	
	if(juniorNode==-1)
		deg2rootexists=false;
	
	int badEdgesLeft=fixBadEdges();
			// estimate the ancestral sequences
	if(badEdgesLeft==-1)
		badFixCount++;		
	
	
	

	
	if(juniorNode==-1) // root+root case
	{
				//create new root...
		T longedgelen=getEdgeLength(subtreeRoot,newedge.end2);
		int newroot=splitEdge(subtreeRoot,newedge.end2);
		setEdgeLength(newroot,subtreeRoot,longedgelen/2);
		setEdgeLength(newroot,newedge.end2,longedgelen/2);
		//cerr<<"\nnewroot:"<<newroot<<":"<<nodes[newroot].printNeighbours();
				// infer and add to Sequences...
		reconstructSeq(newroot,subtreeRoot,newedge.end2);
		nodes[newroot].parent=-1;
		addSeq(newroot);
		rootNodes[nodes[subtreeRoot].myTreeId]=newroot;
	}
			
			// throw away previous versions, put current sequences in the hash tables
			
	//fixBadEdges(); // this way, the root is always there...	
			
	
			
			
			
}

template<class T,int N>	
T PhyloForest<T,N>::getEdgeLength(int node1,int node2)
{
	int pos=nodes[node1].findPos(node2);
	return nodes[node1].bl[pos];
}
		
template<class T,int N>			
void PhyloForest<T,N>::reEstimateQuartetLengths()
{
	for(int i=0;i<edgesAffected.size();i++)
	{
		T newbl=estimateEdgeLength(edgesAffected[i].end1,edgesAffected[i].end2);
		
		if(newbl>0.0)		
			//nodes[edgesAffected[i].end1].setEdgeLength(edgesAffected[i].end2,newbl);
			setEdgeLength(edgesAffected[i].end1,edgesAffected[i].end2,newbl);
		else
		{
			setEdgeLength(edgesAffected[i].end1,edgesAffected[i].end2,minEdgeLen);
		};
		
		
	}
}

template<class T,int N>			
void PhyloForest<T,N>::reEstimateSequences()
{
	for(int i=0;i<nodesAffected.size();i++)
	{
		removeSeq(nodesAffected[i]); //remove from hash tables
		reconstructBasedOnParent(nodesAffected[i]);
		addSeq(nodesAffected[i]);
	}
}
		
template<class T,int N>	
Edge PhyloForest<T,N>::findRepsForEnd(int end1,int end2)
{
	if(nodes[end1].degree==1)
	{
		Edge e1(end1,end1);
		return e1;
	};
	if(nodes[end1].degree==3)
	{
		int pos=nodes[end1].findPos(end2);
		Edge e1(findSeqInDirection(nodes[end1].neighbour[(pos+1)%3],end1),findSeqInDirection(nodes[end1].neighbour[(pos+2)%3],end1));
		return e1;
	};
	Edge err(-1,-1);
	return err;
			
}

template<class T,int N>	
Edge PhyloForest<T,N>::findIndepRepsForEnd(int end1,int end2)
{
	if(nodes[end1].degree==1)
	{
		Edge e1(end1,end1);
		return e1;
	};
	if(nodes[end1].degree==3)
	{
		int pos=nodes[end1].findPos(end2);
		Edge e1(findIndepSeqInDirection(nodes[end1].neighbour[(pos+1)%3],end1),findIndepSeqInDirection(nodes[end1].neighbour[(pos+2)%3],end1));
		return e1;
	};
	Edge err(-1,-1);
	return err;
			
}

template<class T,int N>
int PhyloForest<T,N>::findSeqInDirection(int origin,int forbidden)
{
	if(nodes[origin].seqprobs.size()!=0) // quick hack, just for efficiency
		return origin; 
	BFSCounter++;
	initBFS(origin,BFSCounter);
	BFSBlockExpansion(forbidden,BFSCounter);
	int curNode;
	while( (curNode=BFSStep(BFSCounter))!=-1)
	{
		if(nodes[curNode].seqprobs.size()!=0)
			return curNode;
	}
	return -1;
}

template<class T,int N>
int PhyloForest<T,N>::findIndepSeqInDirection(int origin,int forbidden)
{
	if(nodes[origin].seqprobs.size()!=0&&nodes[origin].parent==forbidden) // quick hack, just for efficiency
		return origin; 
	BFSCounter++;
	initBFS(origin,BFSCounter);
	BFSBlockExpansion(forbidden,BFSCounter);
	int curNode;
	while( (curNode=BFSStep(BFSCounter))!=-1)
	{
		if(nodes[curNode].seqprobs.size()!=0)
		{
			int par=nodes[curNode].parent;
			if(par!=-1&&nodes[par].curBFS==BFSCounter)// we got here from the parent...
				return curNode;
			if(nodes[par].isLeaf)
				return curNode;
		};
	}
	return -1;
}

template<class T,int N>
void PhyloForest<T,N>::resetNode(int node)
{
	nodes[node].seqprobs.clear();
}

template<class T,int N>			
T PhyloForest<T,N>::estimateEdgeLength(int end1,int end2) // for now, just a quartet query, but we might average over many in the future
{
			
	Edge e1=findIndepRepsForEnd(end1,end2);
	Edge e2=findIndepRepsForEnd(end2,end1);
	T len= estimateQuartetMidEdge(e1,e2);
	if(suspEdgeWarning)
	{
		Edge se(end1,end2);
		suspiciousEdges.push_back(se);
	};
	return len;
}


template<class T,int N>			
T PhyloForest<T,N>::estimateQuartetMidEdge(Edge e1, Edge e2) //e1|e2
{
	suspEdgeWarning=false;
	int a=e1.end1;
	int b=e1.end2;
	int c=e2.end1;
	int d=e2.end2;
	
	T d0=distOracle->dist(a,b)+distOracle->dist(c,d);
	T d1=distOracle->dist(a,c)+distOracle->dist(b,d);
	T d2=distOracle->dist(a,d)+distOracle->dist(b,c);
	//cerr<<"quartet tested:"<<a<<","<<b<<" | "<<c<<","<<d<<"\n";
	if(d0>d1||d0>d2)
		suspEdgeWarning=true;
	return ((d1+d2)/2.0-d0)/2.0;
}

template<class T,int N>			
int PhyloForest<T,N>::quartet(int a,int b,int c,int d) //e1|e2
{
	
	T d0=distOracle->dist(a,b)+distOracle->dist(c,d);
	T d1=distOracle->dist(a,c)+distOracle->dist(b,d);
	T d2=distOracle->dist(a,d)+distOracle->dist(b,c);
	
	
	if(d0<d1&&d0<d2)
		return 0;
	if(d1<d2&&d1<d0)
		return 1;
	if(d2<d1&&d2<d0)
		return 2;
	return -1; //tie
}

		
template<class T,int N>	
void PhyloForest<T,N>::updateComponents(int node1,int node2)
{
	int mincomp,maxcolour;
	if(componentSizes[nodes[node1].myTreeId]<componentSizes[nodes[node2].myTreeId])
	{
		mincomp=node1;
		maxcolour=nodes[node2].myTreeId;
	}
	else
	{
		mincomp=node2;
		maxcolour=nodes[node1].myTreeId;
	}
	int mincolour=nodes[mincomp].myTreeId;
	recolourComponent(mincomp,maxcolour);
	componentSizes[maxcolour]+=componentSizes[mincolour];
}
		

template<class T,int N>	
void PhyloForest<T,N>::updateRootInfo(int comp1,int comp2) // sets the root of
{
	if(nodes[rootNodes[comp1]].degree!=2) // we attached something to this one => the other must be the root now. if a new root is about to be created, it doesn't matter
		rootNodes[comp1]=rootNodes[comp2];
	if(nodes[rootNodes[comp2]].degree!=2) // we attached something to this one => the other must be the root now. if a new root is about to be created, it doesn't matter
		rootNodes[comp2]=rootNodes[comp1];
}

template<class T,int N>	
Edge PhyloForest<T,N>::connectTrees(Edge e1,Edge e2)// doesn't add a new root yet
{
	int m1=makeOrFindMidpoint(e1);
	int m2=makeOrFindMidpoint(e2);
			
	//remember original components to update root info
	int comp1=nodes[e1.end1].myTreeId;
	int comp2=nodes[e2.end2].myTreeId;
			// update component information
			
	updateComponents(e1.end1,e2.end2);
			
			// add affected edges
	edgesAffected.clear();
	nodesAffected.clear();
	if(m1!=e1.end1)
	{
		Edge ea(m1,e1.end1);
		Edge eb(m1,e1.end2);
		edgesAffected.push_back(ea);
		edgesAffected.push_back(eb);
	};
	if(m2!=e2.end1)
	{
		Edge ec(m2,e2.end1);
		Edge ed(m2,e2.end2);
		edgesAffected.push_back(ec);
		edgesAffected.push_back(ed);
	};
			
	addEdge(m1,m2);
	
	
	Edge e(m1,m2);
	int junior=findJuniorNode(e);
	if(!nodes[junior].isLeaf&&junior!=-1)
		nodesAffected.push_back(junior);
	int senior=findSeniorNode(e);
	if(senior!=-1)
		nodesAffected.push_back(senior);
			
	edgesAffected.push_back(e);
	
	updateRootInfo(comp1,comp2); //doesn't create a new root yet if it has to be created
	return e;
}
		
		
template<class T,int N>	
int PhyloForest<T,N>::makeOrFindMidpoint(Edge e)
{
	if(e.end1==e.end2)
		return e.end1; // the single sequence case
	for(int i=0;i<nodes[e.end1].degree;i++)
	{
		int neighbour=nodes[e.end1].neighbour[i];
		if(neighbour==e.end2)
		{
			int newnode=splitEdge(e.end1,e.end2);
			return newnode;
		}
		if(nodes[neighbour].degree==2)
		{
			if(nodes[neighbour].neighbour[0]==e.end1&&nodes[neighbour].neighbour[1]==e.end2)
				return neighbour;
			if(nodes[neighbour].neighbour[1]==e.end1&&nodes[neighbour].neighbour[0]==e.end2)
				return neighbour;
		}
				
	}
	return -1;
			
}
		
		
template<class T,int N>			
void PhyloForest<T,N>::recolourComponent(int startNode,int newColour)
{
	BFSCounter++;
	initBFS(startNode,BFSCounter);
	int curNode;
	while((curNode=BFSStep(BFSCounter))!=-1)
		nodes[curNode].myTreeId=newColour;
			
}
		
		
template<class T,int N>			
int PhyloForest<T,N>::reconstructSeq(int node, int child1,int child2)
{
	if(nodes[child1].parent!=node||nodes[child2].parent!=node)
		return -1;// we can't loop
			
	nodes[node].seqprobs.resize(m);
	
	int cpos1=nodes[node].findPos(child1);
	int cpos2=nodes[node].findPos(child2);
	
	SubstitutionMatrix<T,N> Pleft=nodes[node].sm[cpos1];
	SubstitutionMatrix<T,N> Pright=nodes[node].sm[cpos2];
	
	for(int i=0;i<m;i++)
	{
		nodes[node].seqprobs[i]=doCherry(nodes[child1].seqprobs[i],nodes[child2].seqprobs[i],Pleft,Pright);// change here for parsimony
	}
	//nodes[node].parent=nodes[node].findThird(child1,child2);
			
	return 0;
			
}
template<class T,int N>			
int PhyloForest<T,N>::reconstructBasedOnParent(int node)
{
	int child1,child2;
	if(nodes[node].parent==-1)
	{
		child1=nodes[node].neighbour[0];
		child2=nodes[node].neighbour[1];
	}
	else
	{
		int parentpos=nodes[node].findPos(nodes[node].parent);
		child1=nodes[node].neighbour[(parentpos+1)%3];
		child2=nodes[node].neighbour[(parentpos+2)%3];
	}
	return reconstructSeq(node,child1,child2);
}
		
template<class T,int N>	
int PhyloForest<T,N>::addSeq(int node)
{
	vector<int> mapseq=nodes[node].getMAPSeq();
	if(node>=sequences->getNumberOfSequences())
		sequences->addSequence(mapseq);
	else
		sequences->updateSequence(mapseq,node);
	clo->addSeq(node);
	
	distOracle->incSize();
}

template<class T,int N>	
int PhyloForest<T,N>::removeSeq(int node)
{
	if(node>=sequences->getNumberOfSequences())//doesn't exist yet, nothing to remove
		return -1;
	clo->removeSeq(node);
	return 0;
}
		
template<class T,int N>			
int PhyloForest<T,N>::getNeighbouringRoot(int node)//gets a neighbour that is a root, if it exists, or returns node if node is a root
{
	if(nodes[node].degree==2||nodes[node].degree==0)
	{
		return node;
	};
	for(int i=0;i<nodes[node].degree;i++)
	{
		int nbr=nodes[node].neighbour[i];
		if(nodes[nbr].degree==2)
			return nbr;
	}
	return -1;
}
		
template<class T,int N>		
int PhyloForest<T,N>::reconstructMaximalForest()
{
	for(;;)
	{
		DistancePair dp=clo->getClosePair();
		//cerr<<"pair:"<<dp.nodes[0]<<","<<dp.nodes[1]<<"dist:"<<dp.rawdist<<"\n";
		if(getNoComponents()==1||dp.nodes[0]==-1)
			return 0;
				
		if(sameComponent(dp.nodes[0],dp.nodes[1]))
			continue;
				
		int root,nonroot;
		int nr0=getNeighbouringRoot(dp.nodes[0]);
		int nr1=getNeighbouringRoot(dp.nodes[1]);
				
		if(nr0==-1&&nr1==-1)
		{
			if(mergeNonRoots)
				tryMergeNonRoots(dp.nodes[0],dp.nodes[1]);
			else
				waitingCandidates.push_back(dp);
			continue;
		};
		if(nr0!=-1)
		{
			root=nr0; // might be a bit wrong because both may be close to the root, fix later
			nonroot=dp.nodes[1];
		}
		else
		{
			root=nr1; // might be a bit wrong, fix later
			nonroot=dp.nodes[0];
		};
		tryMerge(root,nonroot);
				
	}
	return 0;
}

template<class T,int N>
string PhyloForest<T,N>::printForest()
{
	string forest;
	for(int i=0;i<nodes.size();i++)
		if(nodes[i].degree==2)
			forest+=printTree(i,-1);
	return forest;
}


template<class T,int N>
string PhyloForest<T,N>::printTree(int rootNode,int parent)
{
	string retval;
	if(nodes[rootNode].degree==1)//leaf
	{
		//retval=toString(rootNode);
		retval=sequences->names[rootNode];
		return retval;
	};
	int parentPos;
	if(parent==-1)//root
		parentPos=2;
	else
		parentPos=nodes[rootNode].findPos(parent);
	int first=(parentPos+1)%3;
	int second=(parentPos+2)%3;
	retval="("+printTree(nodes[rootNode].neighbour[first],rootNode)/*+"i"+toString(nodes[rootNode].neighbour[first])*/+":"+toString(nodes[rootNode].bl[first])+",";
	retval+=printTree(nodes[rootNode].neighbour[second],rootNode)/*+"i"+toString(nodes[rootNode].neighbour[second])*/+":"+toString(nodes[rootNode].bl[second])+")";
	if(parent==-1)
		retval+=";\n\n";
	return retval;
}


template<class T,int N>
int PhyloForest<T,N>::swapEdges(int e1end1,int e1end2,int e2end1,int e2end2) // converts that to e1end1<->e2end2, e2end1<->e1end2
{
	nodes[e1end1].swapNeighbour(e1end2,e2end2);
	nodes[e1end2].swapNeighbour(e1end1,e2end1);
	nodes[e2end1].swapNeighbour(e2end2,e1end2);
	nodes[e2end2].swapNeighbour(e2end1,e1end1);
	//swap the parents of e1end1,e2end1
	
	int pswap=nodes[e1end1].parent;
	nodes[e1end1].parent=nodes[e2end1].parent;
	nodes[e2end1].parent=pswap;
	
}

template<class T,int N>
int PhyloForest<T,N>::doNNI(Edge curMidEdge,int iswapNode1,int iswapNode2)// swaps two nodes across a midEdge. must re-estimate the two internal nodes and the edge length
{
	int swapNode1=iswapNode1;
	int swapNode2=iswapNode2;
	int junior=findJuniorNode(curMidEdge);
	int senior=findSeniorNode(curMidEdge);
	if(senior!=-1) // change so that we don't disturb the anc state reconstruction ordering
	{
		if(nodes[senior].parent==iswapNode1||(nodes[senior].parent==-1&&nodes[iswapNode1].parent==-1))// hope this works
		{
			swapNode1=nodes[senior].findThird(junior,iswapNode1);
			swapNode2=nodes[junior].findThird(senior,iswapNode2);
		};
		
		if(nodes[senior].parent==iswapNode2||(nodes[senior].parent==-1&&nodes[iswapNode2].parent==-1))
		{
			swapNode2=nodes[senior].findThird(junior,iswapNode2);
			swapNode1=nodes[junior].findThird(senior,iswapNode1);
		};
		
	};
		
	int endone,endtwo;
	int pos1=nodes[swapNode1].findPos(curMidEdge.end1);
	if(pos1==-1)
		endone=curMidEdge.end2;
	else
		endone=curMidEdge.end1;
	
	int pos2=nodes[swapNode2].findPos(curMidEdge.end1);
	if(pos2==-1)
		endtwo=curMidEdge.end2;
	else
		endtwo=curMidEdge.end1;
	
	if(endone==endtwo) //don't cover other error cases...
		return -1;
	
	swapEdges(swapNode1,endone,swapNode2,endtwo);
	
	// reestimate everything, or at least ask someone to do it
	nodesAffected.clear();
	if(junior==-1)
	{
		nodesAffected.push_back(endone);
		nodesAffected.push_back(endtwo); //ordering doesn't matter here
	}
	else
	{
		nodesAffected.push_back(junior);
		nodesAffected.push_back(senior); // maybe we push the midpoint if it's a long edge? would we even invoke this after a midpoint has been estimated?
	};
	
	resetNode(endone); // so that we don't use obsolete sequences when estimating edge lengths
	resetNode(endtwo);
	//edges
	edgesAffected.clear();
	
	edgesAffected.push_back(curMidEdge); // that's for sure
	
	// not sure if this should be here... I guess it should...
	
	int backbone1=nodes[endone].findThird(endtwo,swapNode2);
	int backbone2=nodes[endtwo].findThird(endone,swapNode1);
	
	Edge e1(endone,backbone1);
	edgesAffected.push_back(e1);
	
	Edge e2(endtwo,backbone2);
	edgesAffected.push_back(e2);
	
	//maybe we should add the swapped edges too? Not sure, depends on how they were estimated in the first place...
	
	Edge e3(endone,swapNode2);
	edgesAffected.push_back(e3);
	
	Edge e4(endtwo,swapNode1);
	edgesAffected.push_back(e4);
	
}

template<class T,int N>
void PhyloForest<T,N>::hideRoot(int component)
{
	int root=rootNodes[component];
	int end1=nodes[root].neighbour[0];
	int end2=nodes[root].neighbour[1];
	
	int pos1=nodes[end1].findPos(root);
	nodes[end1].neighbour[pos1]=end2;
	nodes[end1].parent=-1;
	
	int pos2=nodes[end2].findPos(root);
	nodes[end2].neighbour[pos2]=end1;
	nodes[end2].parent=-1;
	
	setEdgeLength(end1,end2,nodes[root].bl[0]*2);
	
}

template<class T,int N>
void PhyloForest<T,N>::restoreRoot(int component)
{
	int root=rootNodes[component];
	int end1=nodes[root].neighbour[0];
	int end2=nodes[root].neighbour[1];
	
	int pos1=nodes[end1].findPos(end2);
	nodes[end1].neighbour[pos1]=root;
	nodes[end1].parent=root;
	
	int pos2=nodes[end2].findPos(end1);
	nodes[end2].neighbour[pos2]=root;
	nodes[end2].parent=root;
	
	T newlen=nodes[end1].bl[pos1]*0.5;
	setEdgeLength(end1,root,newlen);
	setEdgeLength(end2,root,newlen);
	
	
	
}



template<class T,int N>
bool PhyloForest<T,N>::leafEdge(Edge e)
{
	return nodes[e.end1].isLeaf||nodes[e.end2].isLeaf;
}

template<class T,int N>
int PhyloForest<T,N>::fixBadEdges()// fixes all the edges with negative edge lengths by doing NNI's
{
	nearRootAffected=-1; // so far , nothing	
	bool hiddenRoot=false;
	int component;
	if(suspiciousEdges.size()>0&&deg2rootexists) // hack to get rid of deg 2 vertices
	{
		hiddenRoot=true;
		component=nodes[suspiciousEdges[0].end1].myTreeId;
		hideRoot(component);
	};
	
	int i;
	for(i=0;i<suspiciousEdges.size()&&i<MAX_BAD_FIXES;i++)
	{
		Edge susp=suspiciousEdges[i];
		if(nodes[susp.end1].parent==-1||nodes[susp.end2].parent==-1)
			nearRootAffected=susp.end1;//doesn't matter which end
		if(nodes[susp.end1].findPos(susp.end2)==-1)
			continue; //edge no longer exists
		if(leafEdge(susp))
			continue;
		
		//cerr<<"\n"<<"susp.edge #"<<i<<":"<<susp.printEdge();
		
		int pos1=nodes[susp.end1].findPos(susp.end2);
		int a=nodes[susp.end1].neighbour[(pos1+1)%3];
#ifdef NNI_INDEP_REPS
		a=findIndepSeqInDirection(a,susp.end1); // done for independence...
#endif
		int b=nodes[susp.end1].neighbour[(pos1+2)%3];
#ifdef NNI_INDEP_REPS
		b=findIndepSeqInDirection(b,susp.end1);
#endif
		int pos2=nodes[susp.end2].findPos(susp.end1);
		int c=nodes[susp.end2].neighbour[(pos2+1)%3];
#ifdef NNI_INDEP_REPS
		c=findIndepSeqInDirection(c,susp.end2);
#endif
		int d=nodes[susp.end2].neighbour[(pos2+2)%3];
#ifdef NNI_INDEP_REPS
		d=findIndepSeqInDirection(d,susp.end2);
#endif
		int dir=quartet(a,b,c,d);
		//cerr<<"\nBLAHquartet:"<<a<<","<<b<<","<<c<<","<<d<<"dir"<<dir;
		//if(nodes.size()>40)
		//	cerr<<"\ndegree39:"<<nodes[39].degree<<"\n";
		if(dir==-1) // tie, but both must be better than the negative one which we used to have, I guess
			dir=1+rand()%2;
		
		if(dir==0)
			continue;//edge is ok, for some reason
		if(dir==1)
			doNNI(susp,b,c);
		else
			doNNI(susp,b,d);
		
		reEstimateQuartetLengths();
		reEstimateSequences();
		
		
	}
	
	if(hiddenRoot)
		restoreRoot(component);
	
	if(i==suspiciousEdges.size())//all were considered
		return 0;
	return -1;
}


template<class T,int N>	
void PhyloForest<T,N>::tryMergeNonRoots(int node1,int node2)
{
	if(componentSizes[nodes[node1].myTreeId]>componentSizes[nodes[node2].myTreeId])
	{
		int p= node1;
		node1=node2;
		node2=p;
	};
	vector<Edge> candidates=findCandidateEdges(node2,node1); // find matches in the smaller tree for the bigger tree. we will then reroot and go the other way
			
	// get children instead of the root edge
	Edge e;
	if(nodes[node2].degree<=1)// just do star queries for now, shouldn't make a difference
	{
		e.end1=node2;
		e.end2=node2;
	}
	else
	{
		int pos=nodes[node2].findPos(nodes[node2].parent); //hopefully works for parent==-1
		e.end1=nodes[node2].neighbour[(pos+1)%3];
		e.end2=nodes[node2].neighbour[(pos+2)%3];
	}	
	T minlen=1000000.0;
	int min=-1;
	for(int i=0;i<candidates.size();i++)
	{
		T edgelen=tryAttach(e,candidates[i]);
		
		//cerr<<"\ncand. edge:"<<candidates[i].end1<<","<<candidates[i].end2<<"len:"<<edgelen;
	
		if(edgelen<minlen)   // MUST DO SOME OVERLAP CHECKING AT SOME POINT!
		{
			minlen=edgelen;
			min=i;
		};
	}
	
	// now, reroot the smaller tree...
	int newroot=rerootReconstructions(candidates[min]);
	tryMerge(newroot,node2);
}

template<class T,int N>	
int PhyloForest<T,N>::moveRoot(int node1,int node2)// moves rootId between node1 and node2, acts like splitEdge but moves an existing node and erases its past history
{
	// clear the previous root location
	int component=nodes[node1].myTreeId;
	int newnodeind=rootNodes[component];
	hideRoot(component);
	
	
	int ind1=nodes[node1].findPos(node2);
	int ind2=nodes[node2].findPos(node1);
	
	T edgeLen=nodes[node1].bl[ind1];
	
			
	nodes[node1].neighbour[ind1]=newnodeind;
	nodes[node2].neighbour[ind2]=newnodeind;
	
	nodes[newnodeind].degree=0; // start from scratch
	nodes[newnodeind].addNeighbour(node1);
	nodes[newnodeind].addNeighbour(node2);

	setEdgeLength(newnodeind,node1,edgeLen/2.0);
	setEdgeLength(newnodeind,node2,edgeLen/2.0);
	
				
	
	// anyhow, we have to set the component number
	nodes[newnodeind].myTreeId=nodes[node1].myTreeId;
			
	return newnodeind;
				
}


template<class T,int N>
int PhyloForest<T,N>::rerootReconstructions(Edge rootEdge) // reroots the tree for the purposes of sequence reconstruction. useful for joining non-root nodes...
{
	int root=moveRoot(rootEdge.end1,rootEdge.end2);
	BFSCounter++;
	initBFS(root,BFSCounter);
	vector<int> bfsorder;
	for(;;)
	{
		int next=BFSLookUpNext(BFSCounter);
		if(next==-1)
			break;
		int prev=findBFSParent(next,BFSCounter);
		//bfsorder.push_back(next);
		if(!prev==nodes[next].parent)
			bfsorder.push_back(next);
			//BFSBlockExpansion(next,BFSCounter); //this way we don't re-reconstruct when we don't have to...
		//else
			//bfsorder.push_back(next);
		nodes[next].parent=prev; //hopefully will work for -1
		BFSStep(BFSCounter);
	}
	
	for(int i=bfsorder.size()-1;i>=0;i--)
	{
		if(!nodes[bfsorder[i]].isLeaf)
		{
			reconstructBasedOnParent(bfsorder[i]);
			addSeq(bfsorder[i]);
		};
	}
	return root;
}

// brute-force merging at the end, well, almost brute-force....

template<class T,int N>
void PhyloForest<T,N>::forceJoinAll(int nreps)
{
	vector<vector<int> > reps=findReps(nreps);
	for(int i=0;i<reps.size();i++)
		for(int j=0;j<i;j++)
		{
			for(int indi=0;indi<reps[i].size();indi++)
				for(int indj=0;indj<reps[j].size();indj++)
				{
					double rd=sequences->rawDist(reps[i][indi],reps[j][indj]);
					DistancePair dp(reps[i][indi],reps[j][indj],rd);
					clo->closePairs.push(dp); // HACK HACK HACK, but should work...
				}
				
		}
}

template<class T,int N>
vector<vector<int> > PhyloForest<T,N>::findReps(int nreps)
{
	segmentBorders.clear();
	segmentBorders.resize(nodes.size(),false);
	vector<vector<int> > reps;
	vector<ComponentInfo> existingSizes;
	for(int i=0;i<nodes.size();i++)
	{
		if(nodes[i].degree==2||nodes[i].degree==0) //root of a component
		{
			int comp=nodes[i].myTreeId;
			ComponentInfo ci(comp,componentSizes[comp]);
			existingSizes.push_back(ci);
		};
	}
	
	sort(existingSizes.begin(),existingSizes.end());
	
	for(int i=0;/*i<log(nodes.size())&&*/i<existingSizes.size();i++)
	{
		int nrepshere=(int)((double)nreps*(double)existingSizes[i].size/ntaxa);
		if(nrepshere==0)
			nrepshere=1;
		vector<int> repshere=decomposeComponent(rootNodes[existingSizes[i].comp],nrepshere);
		reps.push_back(repshere);
	}
		
	return reps;
}


template<class T,int N>
vector<int> PhyloForest<T,N>::decomposeComponent(int startNode,int nreps)
{
	vector<int> reps;
	if(componentSizes[nodes[startNode].myTreeId]<=1)
	{
		reps.push_back(startNode);
		return reps;
	};
	int i=0;
	queue<int> stNodes;
	stNodes.push(startNode);
	while(i<nreps)
	{
		if(stNodes.empty())
			break;
		int curNode=stNodes.front();
		stNodes.pop();
		int median=findSegmentMedian(startNode);
		segmentBorders[median]=true;
		for(int j=0;j<nodes[median].degree;j++)
			if(!segmentBorders[nodes[median].neighbour[j]])
				stNodes.push(nodes[median].neighbour[j]);
		reps.push_back(median);
		i++;
	}
	return reps;
}

template<class T,int N>
int PhyloForest<T,N>::findSegmentMedian(int startNode)
{
	int onefar=findFurthestInSegment(startNode);
	int twofar=findFurthestInSegment(onefar);
	int median=findHalfWay(twofar);
	segmentBorders[median]=true;
	return median;
}

template<class T,int N>
int PhyloForest<T,N>::findFurthestInSegment(int startNode)
{
	BFSCounter++;
	initBFS(startNode,BFSCounter);
	T furthestDist=0.0;
	int furthest=-1;
	BFSStep(BFSCounter);
	for(;;)
	{
		int next=BFSLookUpNext(BFSCounter);
		if(next==-1)
			break;
		int prev=findBFSParent(next,BFSCounter);
		if(!segmentBorders[prev])
			nodes[next].distFromBFSOrigin=nodes[prev].distFromBFSOrigin+nodes[next].bl[nodes[next].findPos(prev)];
		else
			nodes[next].distFromBFSOrigin=100000.0; // see how that works
		if(segmentBorders[next])
		{
			BFSBlockExpansion(next,BFSCounter);
			for(int i=0;i<nodes[next].degree;i++)
				if(nodes[next].neighbour[i]!=prev)
					nodes[next].distFromBFSOrigin=100000.0;
		}
		BFSStep(BFSCounter); 
		
		if(nodes[next].distFromBFSOrigin>furthestDist)
		{
			furthest=next;
			furthestDist=nodes[next].distFromBFSOrigin;
		};
	}
	return furthest;
	
}

template<class T,int N>
int PhyloForest<T,N>::findHalfWay(int endNode)// relies on no BFS searches having occurred after findFurthest...
{
	
	T dist=nodes[endNode].distFromBFSOrigin;
	int bfsid=nodes[endNode].curBFS;
	T curDist=dist;
	int curNode=endNode;
	while(nodes[curNode].distFromBFSOrigin>=dist/2.0)
	{
		for(int i=0;i<nodes[curNode].degree;i++)
		{
			int nbr=nodes[curNode].neighbour[i];
			if(nodes[nbr].curBFS!=bfsid)
				continue;
			if(nodes[nbr].distFromBFSOrigin<nodes[curNode].distFromBFSOrigin)
			{
				curNode=nbr;
				break;
			};
		}
	}
	return curNode;
	
}


// template<class T,int N>
// bool PhyloForest<T,N>::isNearRoot(int x)// returns true if there is no node that has x as child, for reconstruction purposes
// {
// 	if(nodes[x].parent==-1)
// 		return true;
// 	int parent=nodes[x].parent;
// 	if(nodes[parent].parent==x)
// 		return x;
// }

int main(int argc,char** argv)
{
	if(argc < 2){
		cerr<<"Usage: "<<argv[0]<<" fasta_file\n";
		exit(0);
	}
	BitSequences seqs;
	string fasta=argv[1];
	seqs.read(fasta);
	
	PhyloForest<double,4> pf;
	srand(37);
	pf.init(seqs.sequences.size(),&seqs);
	
	pf.mergeNonRoots=true;
	
	pf.reconstructMaximalForest();
	
	pf.clo->resetHashTables(4,4,0.2,0.5);
	
	pf.mergeNonRoots=true;
	
	pf.forceJoinAll(sqrt(seqs.getNumberOfSequences()));
	
	pf.reconstructMaximalForest();
	
	cerr<<"badFixCount="<<pf.badFixCount<<"\n";
	cout<<pf.printForest();
	
	
};
		
		
		