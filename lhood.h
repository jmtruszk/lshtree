#ifndef LHOOD_H
#define LHOOD_H

#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<fstream>
#include<cmath>
#include<sstream>
#include<unistd.h>


#include"seqs.h"
#include"lshtable.h"

const int MAX_BAD_FIXES=50; //max bad edge fixes

// used with lhood5.cpp...
using namespace std;

template<class T, int N>
class ClosenessOracle;
class DistancePair;

struct Edge
{
	int end1;
	int end2;
	
	Edge()
	{
	}
	Edge(int n1,int n2)
	{
		end1=n1;
		end2=n2;
	}
	
	string printEdge()
	{
		stringstream ss;
		ss<<end1<<","<<end2;
		return ss.str();
	}
};



template <class T,int N> // scale now in log form!
struct ProbVector
{
	T probs[N];
	T scale;
	
	void initLetter(int letter)
	{
		if(letter==GAP_VALUE)
		{
			for(int i=0;i<N;i++)
				probs[i]=1.0/N;
			return;
		};
		for(int i=0;i<N;i++)
			probs[i]=0.0;
		probs[letter]=1.0;
		scale=0.0;
		//scale=1.0;
	}
	
	void rescale() //rescales everything so that probs sum to 1 (they actually sum to scale
	{
		T sumprob=0.0;
		
		for(int i=0;i<N;i++)
		{
			sumprob+=probs[i];
		}
		
		for(int i=0;i<N;i++)
		{
			probs[i]=probs[i]/sumprob;
		}
		T logsumprob=log(sumprob);
		scale=scale+logsumprob;
		//scale=scale*sumprob;
		
	}
	
	int argmax()// useful for the most likely letter - for hash tables
	{
		int maxes[4];
		maxes[0]=0;
		int nmaxes=1;
		int amax=0;
		for(int i=0;i<N;i++)
		{
			if(probs[i]>probs[amax])
			{
				amax=i;
				nmaxes=1;
				maxes[0]=i;
				continue;
			};
			if(probs[i]==probs[amax])
			{
				maxes[nmaxes]=i;
				nmaxes++;
				continue;
			};
			
		}
		return maxes[rand()%nmaxes];
		//return amax;
	}
	
	string print()
	{
		stringstream ss;
		ss<<"[";
		for(int i=0;i<N;i++)
		{
			ss<<probs[i];
			ss<<",";
		}
		ss<<"]";
		return ss.str();
	}
	
	
};


template <class T,int N>
struct SubstitutionMatrix
{
	T matrix[N][N];
	
	void createJK69(T branchlength)// works only for 4x4 for now!
	{
		T resample=1.0-exp(-(4/3)*(double)branchlength);
		T onedir=0.25*resample;
		T staysame=1.0-3*onedir;
		for(int i=0;i<N;i++)
		{
			for(int j=0;j<N;j++)
			{
				matrix[i][j]=onedir;
			}
			matrix[i][i]=staysame;
		}
	}
};


template<class T,int N>
ProbVector<T,N> operator*(ProbVector<T,N> p,SubstitutionMatrix<T,N> M)
{
	ProbVector<T,N> result;
	for(int i=0;i<N;i++)
	{
		result.probs[i]=0.0;
		for(int j=0;j<N;j++)
		{
			result.probs[i]+=(p.probs[j]*M.matrix[j][i]);
		}
	}
	result.scale=p.scale;
	return result;
}

template<class T,int N>
ProbVector<T,N> operator*(ProbVector<T,N> p,ProbVector<T,N> q)
{
	ProbVector<T,N> result;
	for(int i=0;i<N;i++)
	{
		result.probs[i]=p.probs[i]*q.probs[i];
	}
	result.scale=p.scale+q.scale;
	//result.scale=p.scale*q.scale;
	return result;
}


template<class T,int N>
ProbVector<T,N> doCherry(ProbVector<T,N> left,ProbVector<T,N> right,SubstitutionMatrix<T,N> Pleft, SubstitutionMatrix<T,N> Pright)
{
	
	ProbVector<T,N> left2=left*Pleft;
	ProbVector<T,N> right2=right*Pright;
		
	ProbVector<T,N> final=left2*right2;
	final.rescale();// WATCH OUT FOR THIS!!! HACK!!! SOME DATA TYPES MAY SELF-RESCALE!
	return final;
}
	
template<class T,int N>
ProbVector<T,N> doCherryParsimony(ProbVector<T,N> left,ProbVector<T,N> right,SubstitutionMatrix<T,N> Pleft, SubstitutionMatrix<T,N> Pright)
{
	int counts[N];
	for(int i=0;i<N;i++)
		counts[i]=0;
	for(int i=0;i<N;i++)
	{
		if(left.probs[i]>0.0)
			counts[i]++;
		if(right.probs[i]>0.0)
			counts[i]++;
	}
	int nmax=0;
	for(int i=0;i<N;i++)
		if(counts[i]>nmax)
			nmax=counts[i];
	
	ProbVector<T,N> final=left*right;
	for(int i=0;i<N;i++)
		if(counts[i]==nmax)
			final.probs[i]=1.0;
	return final;
}

const double minEdgeLen=0.0001;

template<class T,int N>
struct PhyloForestNode
{
	
	vector<ProbVector<T,N> > seqprobs; // possibly useful, for 3-way reconstruction... most of them probably won't be used
	int parent; //parents for seqprobs
	
	T bl[3]; // branch length
	
	SubstitutionMatrix<T,N> sm[3];
	
	int neighbour[3];
	int degree;
	
	int myid;
	int myTreeId;
	
	int curBFS; // indicates the # of the BFS that last visited this node
	T distFromBFSOrigin;
	
	bool isLeaf;
	
	PhyloForestNode<T,N>()
	{
		degree=0;
		parent=-1;
		isLeaf=false;
	}
	
	
	vector<int> getMAPSeq()
	{
		vector<int> mapseq;
		for(int i=0;i<seqprobs.size();i++)
		{
			mapseq.push_back(seqprobs[i].argmax());
		}
		return mapseq;
	}
	
	int findPos(int neighbourid)
	{
		for(int i=0;i<degree;i++)
			if(neighbourid==neighbour[i])
				return i;
		return -1;
	}
	
	int findThird(int first,int second)
	{
		if(degree<3)
			return -1;
		int fpos=findPos(first);
		if(neighbour[(fpos+1)%3]==second)
			return neighbour[(fpos+2)%3];
		return neighbour[(fpos+1)%3];
		
	}
	
	int clearProbs()
	{
		seqprobs.clear();
	}
	
	void initProbs(vector<int> seq)
	{
		seqprobs.resize(seq.size());
		for(int i=0;i<seq.size();i++)
			seqprobs[i].initLetter(seq[i]);
	}
	
	void initLeaf(int me,const vector<int>& seq)
	{
		//vector<int> seq2=seq;
		myid=me;
		myTreeId=me;
		degree=0;
		isLeaf=true;
		parent=-1;
		//initProbs(seq2);
		seqprobs.resize(seq.size());
		for(int i=0;i<seq.size();i++)
			seqprobs[i].initLetter(seq[i]);
	}
	
	int addNeighbour(int newneighbour)
	{
		if(degree>=3)
			return -1;
		neighbour[degree]=newneighbour;
		degree++;
		
	}
	
	void setEdgeLength(int neighbourid,T edgelen)
	{
		int pos=findPos(neighbourid);
		bl[pos]=edgelen;
		sm[pos].createJK69(edgelen);
	}
	
	string printNeighbours()
	{
		stringstream ss;
		ss<<"neighbours:";
		for(int i=0;i<degree;i++)
		{
			ss<<neighbour[i];
			ss<<",";
		}
		return ss.str();
	}
	
	int swapNeighbour(int prev,int cur)
	{
		int pos=findPos(prev);
		if(pos==-1)
			return -1;
		neighbour[pos]=cur;
	}
	
};


struct ComponentInfo
{
	int comp;
	int size;
	
	ComponentInfo(int c,int s)
	{
		comp=c;
		size=s;
	}
};

bool operator<(ComponentInfo a,ComponentInfo b)
{
	return a.size>b.size; //sort from the largest
}

template<class T,int N>
class PhyloForest
{
	public:
		BitSequences* sequences; // leaves+ ancestral MAP's
		ClosenessOracle<T,N>* clo;
		
		DistanceOracle* distOracle;
		//QuartetOracle* Q;
		
		vector<PhyloForestNode<T,N> > nodes;
		int ntaxa;
		
		vector<int> componentSizes;
		vector<int> rootNodes; // by component, only for non-leaves
		
		//doesn't work for now
		//vector<bool> activeComponent;
		bool deg2rootexists;
		
		int m;// seq length
		
		bool mergeNonRoots;
		
		bool suspEdgeWarning;
		
		int badFixCount;
		// for BFS searches
		
		int BFSCounter;
		queue<int> bfsqueue;
		
		//for estimating/reestimating branch lengths/ancestral sequences
		
		vector<Edge> edgesAffected;
		vector<int> nodesAffected;
		
		vector<Edge> suspiciousEdges;
		
		int nearRootAffected;
		
		vector<DistancePair> waitingCandidates;
		
		vector<bool> segmentBorders;// for decomposition in heuristic search
		
		void init(int ntaxa,BitSequences* seqs);
		void initNoClo(int ntaxa,BitSequences* seqs);
		void initClo();
		
		
		
		void initProbs();
		void initLeaves();
		
		
		void initBFS(int startNode,int searchId); // searchId distinguishes this BFS from all the previous ones
		
		int BFSStep(int searchId); //returns the next node encountered 
		
		int BFSStepWithForbiddenExpansions(int searchId,vector<bool>& forbiddenExpansions);
		
		
		int sameComponent(int node1,int node2);
		
		// doesn't work for now
		//int checkSumComponentSizes();
		int BFSLookUpNext(int searchId);// without fetching from the queue
		
		
		int BFSBlockExpansion(int node,int searchId); // prevents going further from node
		
		
		int findBFSParent(int curNode,int searchId); //works only when curNode has not been expanded yet
		
		
		
		
		int findPotentialParent(int node); // really, find two neighbours whose reconstructed sequences do not depend on this one, choose the third direction
		
		int splitEdge(int node1,int node2); // after this, one has to set edge lengths and the ancestral sequence
		
		
		void addEdge(int node1,int node2);
		
		void setEdgeLength(int node1,int node2,T len);
		
		int findJuniorNode(Edge e);
		
		int findSeniorNode(Edge e);
		
		
		
		
		vector<Edge> findCandidateEdges(int subtreeRoot,int otherHit);
		T tryAttach(Edge e1,Edge e2);
		
		Edge getLongEdgeFromRoot(int root);
		
		
		void tryMerge(int subtreeRoot,int otherHit);
		
		T getEdgeLength(int node1,int node2);
		
		
		void reEstimateQuartetLengths();
		
		void reEstimateSequences();
		
		Edge findRepsForEnd(int end1,int end2);
		
		Edge findIndepRepsForEnd(int end1,int end2);
		
		int findSeqInDirection(int origin,int forbidden);
		
		int findIndepSeqInDirection(int origin,int forbidden);
		
		void resetNode(int node);
		
		T estimateEdgeLength(int end1,int end2) ;// for now, just a quartet query, but we might average over many in the future
		
		T estimateQuartetMidEdge(Edge e1, Edge e2); //e1|e2
		
		
		void updateComponents(int node1,int node2);
		
		Edge connectTrees(Edge e1,Edge e2);
		
		int makeOrFindMidpoint(Edge e);
		
		
		
		void recolourComponent(int startNode,int newColour);
		
		
		
		
		int reconstructSeq(int node, int child1,int child2);
		
		int reconstructBasedOnParent(int node);
		
		
		int addSeq(int node);
		
		int removeSeq(int node);
		
		
		int getNeighbouringRoot(int node);
		
		int reconstructMaximalForest();
		
		string printForest();
		string printTree(int,int);
		
		int swapEdges(int,int,int,int);
		// fixing reconstruction errors
		
		int doNNI(Edge curMidEdge,int swapNode1,int swapNode2);
		
		void hideRoot(int);
		void restoreRoot(int);
		
		int fixBadEdges();
		
		int quartet(int,int,int,int);
		
		Edge findRootEdgeIfNecessary(Edge newedge);
		
		bool leafEdge(Edge);
		
		void updateRootInfo(int,int);
		
		
		// for non-root - non-root merges
		void tryMergeNonRoots(int node1,int node2);
		
		int rerootReconstructions(Edge rootEdge);
		
		int moveRoot(int,int);
		
		//for heuristic search 
		
		void forceJoinAll(int nreps);
		vector<vector<int> > findReps(int nreps);
		
		vector<int> decomposeComponent(int startNode,int nreps);
		
		int findSegmentMedian(int startNode);
		
		int findFurthestInSegment(int startNode);
		
		int findHalfWay(int endNode);
		
		
		int getNoComponents()// quick and dirty way to get the number of components, may not work in the middle of a merge
		{
			return 2*ntaxa-nodes.size();
		}
		
		// rate estimation
		
		int reconstructSiteWithRelRate(int node, int child1,int child2,int site,T relrate);
		
		int reconstructSiteBasedOnParent(int node,int site,T relrate);
		
		vector<int> getPostOrder(int root);
		
		T lhoodForSite(int site,T relrate,vector<int>& bfsorder);
		
		T estimateLhoodForSite(int site,vector<int>& bfsorder);
		
		vector<T> estimateLhoods(int root);
		
		void reconstructAncestralSeqsSoFar();
		// parsing
		
		vector<string> tokenizeNewick(char* newick);
		void readTree(char* newick);
		
		//experiment
		vector<int> selectTaxaToDelete(int ndels);
		vector<int> selectTaxaToSave(int ndels);
		bool hasMarkedSibling(int taxon,vector<bool>marked);
		
		void disconnectTaxon(int taxon);
		
		
		
};
#endif