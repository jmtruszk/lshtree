#ifndef SEQS_H
#define SEQS_H
#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<fstream>
#include<cmath>
#include<sstream>
#include<unistd.h>

#include<ext/hash_map>


#ifndef _GAPVAL
#define _GAPVAL
const int GAP_VALUE=1000000;
#endif







// for mem management
const int MAX_HT_SIZE=12000000;

//almost synonyms

const int QUASI_SYNONYM_THRESHOLD=8;


using namespace std;


class Sequences;

static char REVTAB[24]=
{
	'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V','B','Z','X','*'
};

static char REVTABNUC[4]=
{
	'A','C','G','T'
};
// purines are A and G, 00 and 10. We could use that for better hashing?

class Sequences
{
	public:
		int ALPHABET[256];
	
		vector<vector<int> > sequences;
		vector<string> names;
	
	
		Sequences();
	
		int read(string file);
	
		int getNumberOfSequences();
	
		bool isGap(int seqno,int pos);
	
		void appendSequence(string seq) ;// converts the sequence to ints and appends it at the end of the sequence list

};

static const int bitmask[]={ 0xC000,0x3000,0x0C00,0x0300,0x00C0,0x0030,0x000C,0x0003 };

class BitSequences: public Sequences
{
	public:
		
		int ALPHABET[256];
		int nDiffs[65536];
	
		vector<vector<unsigned short int> > sequences;
		vector<vector<unsigned short int> > gaps; // changed! 1 means NO GAP
		//vector<string> names;
	
	
		BitSequences();
	
		int read(string file);
	
		int getNumberOfSequences();
		
		int getNumberOfColumns();
	
		bool isGap(int seqno,int pos);
	
		void appendSequence(string seq);
		
		
		void updateSequence(vector<int> seq,int index);
		
		void addSequence(vector<int> seq);
		
		double rawDist(int a,int b);
		// for backward compatibility -quasi synonym checking
		double hammingDist(int a,int b);
		
		
		// for HT's
		
		int getSubseq(int seqno, vector<int> positions);
		
		int getLetter(int seqno,int position) // for now, no gap checking/randomizing
		{
			int block=position>>3;
			int offset=position&0x00000007;
			unsigned short int mask=bitmask[offset];
			int letter=(sequences[seqno][block]&mask)>>((7-offset)*2);
			return letter;
			
		}
		
		vector<int> getSequence(int seqno);
};




struct seqpair
{
	int a;
	int b;
};



// struct HashFunction
// {
// 	size_t operator()(const seqpair& s) const
// 	{
// 		return s.a<<log2TotalTaxaNo+s.b;
// 	}
// 	bool operator()(const seqpair& s1,const seqpair& s2) const
// 	{
// 		return (s1.a==s2.a)&&(s1.b==s2.b);
// 	}
// };

// ver 28 -- speed up by caching distances already computed... hope it pays off
//v38 -blosum introduced
//v41 to do: annihilating duplicate sequences

// v108 - moved to BitSequences, raw distance calculation delegated to BitSequences - works only for nucleotides for now
class DistanceOracle
{
	public:
		//__gnu_cxx::hash_map<seqpair,double,HashFunction,HashFunction> distCache;
		vector<__gnu_cxx::hash_map<int,double> > distCache;
		//PairHash distCache;
		bool correction;
		//bpp::AlignedSequenceContainer sequences;
		//we need to read stuff ourselves otherwise it will keep crashing
		BitSequences* sequences;
		bool self_alloc_sequences;
		
		int ndistqueries; // queries asked so far
		bool nanerror;
		int whoseturnclear;
		DistanceOracle(string file,bool corr);
		DistanceOracle(BitSequences* seqs);
		
		~DistanceOracle();
		
		int incSize();

		bool hashCheck(int a,int b);
		
		double hashGet(int a,int b);
		
		void hashSet(int a,int b,double d);
		
		void hashClear();
		
		void hashHalfClear();
		
		double dist(int a,int b);
		

		

		
		bool quasiSynonymCheck(int a,int b);
		
		vector<vector<double> > calcAll();
		
		void printToPhylip(string file);
		
// 		double dist(int a,int b)
// 		{
// 			//if(!correction)
// 			//	return rawDist(a,b);
// 			double d=rawDist(a,b);
// 			double adjusted=1.0 - d - (d * d * 0.20) ;
// 			if(adjusted<0)
// 			{
		// 				
// 				cerr<<"ERROR: distance correction error "<<adjusted;
// 				cerr<<"raw distance:"<<d;
// 				cerr<<"sequences:"<<a<<","<<b;
// 				exit(0);
// 			};
// 			double distance = - log( adjusted );
// 			return distance;
// 		}
		
		int size();
	
};

#endif
