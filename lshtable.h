#ifndef LSHTABLE_H
#define LSHTABLE_H

#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<fstream>
#include<cmath>
#include<sstream>
#include<unistd.h>

#include"seqs.h"
#include"lhood.h"

const double gapThreshold=0.05; // at least this % non-gaps in a column

const double minMutation=0.1;
const double maxMutation=0.3;

template<class T,int N>
class PhyloForest;

class ColumnSelector
{
	public:
		BitSequences* seqs;
		
		vector<int> lowGapPositions;
		
		ColumnSelector(BitSequences* s)
		{
			seqs=s;
		}
		
		void findLowGapPositions(double threshold)
		{
			lowGapPositions.clear();
			for(int i=0;i<seqs->getNumberOfColumns();i++)
			{
				if(evalGapPercentage(i)<threshold)
					lowGapPositions.push_back(i);
			}
		}
		
		double evalGapPercentage(int site)
		{
			int ngaps=0;
			for(int i=0;i<seqs->getNumberOfSequences();i++)
			{
				ngaps+=seqs->isGap(i,site);
			}
			return (double)ngaps/(double)seqs->getNumberOfSequences();
		}
		
		int drawRandomGoodColumn()
		{
			return lowGapPositions[rand()%lowGapPositions.size()];
		}
		
		vector<int> drawGoodBootstrapSample(int len)
		{
			vector<int> positions;
			for(int i=0;i<len;i++)
				positions.push_back(drawRandomGoodColumn());
			return positions;
		}
		
};

const int MAX_KEY_LEN=12; // 12 letters==2^24 buckets



class LSHTable;
const int maxEntrySize=100;

struct LSHEntry
{
	LSHTable* table;
	bool nested;
	
	vector<int> hits; //maybe it's better to have vector<vector>? for extra fidelity? but that can be done with a nested hash table...
	
	
	int add(int seqno);
	
	void addNewHT(BitSequences* seqs,vector<int> positions);
	
	LSHEntry();
	
	LSHEntry(const LSHEntry& le);
	
	~LSHEntry();
};



class LSHTable
{
	public:
		vector<int> positions;
		BitSequences* seqs;
		
		int nbuckets;
		int goalKeyLen; // what it's supposed to be in total
		int* mainHT; //contains pointers to lists of hits or to lower-tier hash tables, if need be
		bool mainHTalloc;
		
		vector<LSHEntry> entries; // where mainHT points at
		
		LSHTable()
		{
			mainHTalloc=false;
		}
		
		LSHTable(BitSequences* sequences,vector<int> positionsvec)
		{
			positions=positionsvec;
			goalKeyLen=positionsvec.size();
			if(positions.size()>MAX_KEY_LEN)
				positions.resize(MAX_KEY_LEN);
			seqs=sequences;
			nbuckets=(int)pow((double)4,(double)positions.size());
			mainHT=new int[nbuckets];
			mainHTalloc=true;
			for(int i=0;i<nbuckets;i++)
				mainHT[i]=-1;
		}
		
		~LSHTable()
		{
			if(mainHTalloc)
				delete mainHT;
		}
		
		LSHTable(const LSHTable& lt)
		{
			positions=lt.positions;
			seqs=lt.seqs;
			nbuckets=lt.nbuckets;
			goalKeyLen=lt.goalKeyLen;
			entries=lt.entries;
			if(lt.mainHTalloc)
			{
				mainHTalloc=true;
				mainHT=new int[nbuckets];
				for(int i=0;i<nbuckets;i++)
					mainHT[i]=lt.mainHT[i];
			}
			else
				mainHTalloc=false;
		}
		
		void init(BitSequences* sequences,vector<int> positionsvec)
		{
			positions=positionsvec;
			goalKeyLen=positionsvec.size();
			if(positions.size()>MAX_KEY_LEN)
				positions.resize(MAX_KEY_LEN);
			seqs=sequences;
			nbuckets=(int)pow((double)4,(double)positions.size());
			mainHT=new int[nbuckets];
			mainHTalloc=true;
			for(int i=0;i<nbuckets;i++)
				mainHT[i]=-1;
		}
		
		int hashSequence(int seqno)
		{
			int key=seqs->getSubseq(seqno,positions);
			//cerr<<"seqno:"<<seqno<<"key:"<<key<<"\n";
			if(mainHT[key]==-1)//new entry needed
			{
				LSHEntry entry;
				entry.add(seqno);
				entries.push_back(entry);
				mainHT[key]=entries.size()-1;
				
				return 0;
			};
			entries[mainHT[key]].add(seqno);
			return 1;
		}
		
		int getEntry(int seqno)
		{
			return mainHT[seqs->getSubseq(seqno,positions)];
		}
		
		LSHEntry* getFinalEntry(int seqno)
		{
			int entryid=mainHT[seqs->getSubseq(seqno,positions)];
			if(entries[entryid].nested)
				return entries[entryid].table->getFinalEntry(seqno);
			return &entries[entryid];
		}
		
		
		
};








struct DistancePair
{
	int nodes[2];
	double rawdist;
	DistancePair(int n1,int n2,double rd)
	{
		nodes[0]=n1;
		nodes[1]=n2;
		rawdist=rd;
	}
};

bool operator<(DistancePair a,DistancePair b);


const int minHTs=7;

template<class T,int N>
class ClosenessOracle
{
	public:
		vector<LSHTable> tables;
		
		BitSequences* seqs;
		
		PhyloForest<T,N>* pf;
		
		ColumnSelector* cs;
		
		priority_queue<DistancePair> closePairs;
		
		double pmin,pmax,padd;
		
		int maxHT,maxKeyLen;
		
		
		int init(BitSequences* s,PhyloForest<T,N>* phf,double pmin,double pmax);
		
		void resetHashTables(int nHTs,int maxHTs,double pmin,double pmax);
		
		int calcKeyLen(double pmin,double pmax);
		
		int calcNoHashTables(double pmin,double pmax);
		
		
		void addSeq(int seqno);
		
		void addSeqToTable(int seqno,int i);
		
		void removeSeq(int seqno);
		
		void removeSeqFromTable(int seqno,int i);
		
		DistancePair getClosePair();
		
		
		void addHashTable();
		
		
		
		
		
};






//definitions!

template<class T,int N>
int ClosenessOracle<T,N>::init(BitSequences* s,PhyloForest<T,N>* phf,double pmin,double pmax)
{
	seqs=s;
	pf=phf;
	maxHT=calcNoHashTables(pmin,pmax);
	cerr<<"max # HT's:"<<maxHT;
	maxKeyLen=calcKeyLen(pmin,pmax);
	cs=new ColumnSelector(s);
	cs->findLowGapPositions(gapThreshold);
	tables.reserve(minHTs);
	for(int i=0;i<minHTs;i++)
	{
		addHashTable();
	}
			
}


template<class T,int N>
void ClosenessOracle<T,N>::resetHashTables(int nHTs,int maxHTs,double pmin,double pmax)
{
	maxHT=maxHTs;
	cerr<<"max # HT's:"<<maxHT;
	maxKeyLen=calcKeyLen(pmin,pmax);
	tables.clear();
	//cs=new ColumnSelector(s);
	//cs->findLowGapPositions(gapThreshold);
	tables.reserve(nHTs);
	for(int i=0;i<nHTs;i++)
	{
		addHashTable();
	}
			
}

template<class T,int N>
int ClosenessOracle<T,N>::calcKeyLen(double pmin,double pmax)
{
			
	this->pmin=pmin;
	this->pmax=pmax;
	padd=pmin+(pmax-pmin)/2; //heuristic...
	int ntaxa=seqs->getNumberOfSequences();
	double P1=1.0-pmin;
	double P2=1.0-pmax;
	double ratio=P1/P2;
	double keylen=log((double)ntaxa)/log(1.0/P2); //changed from P1/P2
	return (int)keylen;
}


template<class T,int N>
int ClosenessOracle<T,N>::calcNoHashTables(double pmin,double pmax)
{
	this->pmin=pmin;
	this->pmax=pmax;
	padd=pmin+(pmax-pmin)/2; //heuristic...
	int ntaxa=seqs->getNumberOfSequences();
	double nhash=pow(ntaxa*2,pmin/pmax)*2/* *log(ntaxa)*/+1; // changed to *log to have whp...see how that goes...now it's *2
	if(nhash>12)
		nhash=12.0;
	return (int) nhash;
}
		
template<class T,int N>		
void ClosenessOracle<T,N>::addSeq(int seqno)
{
	for(int i=0;i<tables.size();i++)
		addSeqToTable(seqno,i);
	if(closePairs.size()>16000000) //HACK! too big... reset HT's
	{
		double newpmax=pmax-0.03; // very gently, probably not even needed
		int newMaxHT=calcNoHashTables(pmin,newpmax);
		while(!closePairs.empty())
			closePairs.pop();
		resetHashTables(tables.size(),newMaxHT,pmin,newpmax);
	};
}

template<class T,int N>		
void ClosenessOracle<T,N>::addSeqToTable(int seqno,int i)// assumes we have added it to seqs
{
			
	tables[i].hashSequence(seqno);
	LSHEntry* entry=tables[i].getFinalEntry(seqno);
	vector<DistancePair> v;
	for(int j=0;j<entry->hits.size();j++)
	{
		if(!pf->sameComponent(seqno,entry->hits[j]))
		{
			double rd=seqs->rawDist(seqno,entry->hits[j]);
			DistancePair dp(seqno,entry->hits[j],rd);
			v.push_back(dp);
		};
	}
	if(v.size()>100)
	{
		sort(v.begin(),v.end());
		for(int k=0;k<30;k++)
			closePairs.push(v[k]);
	}
	else
	{
		for(int k=0;k<v.size();k++)
			closePairs.push(v[k]);
	}	
}

template<class T,int N>		
void ClosenessOracle<T,N>::removeSeq(int seqno)
{
	for(int i=0;i<tables.size();i++)
		removeSeqFromTable(seqno,i);
}

template<class T,int N>		
void ClosenessOracle<T,N>::removeSeqFromTable(int seqno,int i)// assumes we have added it to seqs
{	
	LSHEntry* entry=tables[i].getFinalEntry(seqno);
	vector<int>::iterator it=find(entry->hits.begin(),entry->hits.end(),seqno);
	entry->hits.erase(it); // we might add deleting from the heap later . or something more efficient here...
}




template<class T,int N>		
DistancePair ClosenessOracle<T,N>::getClosePair()
{
	if(closePairs.empty())
	{
		DistancePair err(-1,-1,10000.0);
		return err;
	};
			
	DistancePair dp=closePairs.top();
	closePairs.pop();
	if(dp.rawdist>padd&&tables.size()<maxHT)
		addHashTable();
	return dp;
}
		
template<class T,int N>	
void ClosenessOracle<T,N>::addHashTable()
{
	int htsize=calcKeyLen(pmin,pmax);
	cerr<<"htsize:"<<htsize<<"\n";
	vector<int> positions=cs->drawGoodBootstrapSample(htsize);
	LSHTable lsht;
	tables.push_back(lsht);
	tables[tables.size()-1].init(seqs,positions);
	for(int i=0;i<seqs->getNumberOfSequences();i++)
	{
		addSeqToTable(i,tables.size()-1);
	}
}


LSHEntry::LSHEntry()
{
	nested=false;
}


LSHEntry::LSHEntry(const LSHEntry& le)
{
	hits=le.hits;
	nested=le.nested;
	if(nested)
	{
		table=new LSHTable();
		table=le.table;
	};
}



int LSHEntry::add(int seqno)
{
	if(nested)
	{
		table->hashSequence(seqno);
		return 1;
	};
	hits.push_back(seqno);
	return 0;
}
	
void LSHEntry::addNewHT(BitSequences* seqs,vector<int> positions)
{
	nested=true;
	table=new LSHTable(seqs,positions);
	for(int i=0;i<hits.size();i++)
		table->hashSequence(hits[i]);
}
	
LSHEntry::~LSHEntry()
{
	if(nested)
		delete table;
}


//random useful functions

bool operator<(DistancePair a,DistancePair b)
{
	return a.rawdist>b.rawdist; // THIS IS A HACK TO MAKE A MIN-HEAP FROM A MAX-HEAP
}


#endif